# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 12:29:18 2019

@author: kpodo
"""
import numpy as np
import pandas as pd 
import csv
import glob
import os


fileFrom = 'from.xlsx'  
fileTo   = 'to.xlsx'

def MakeBigData(file):
    pass
    
def mergeFromEnvWeather(fileFrom, fileTo):
    """
    This function takes all the vars (atmp_max,min, etc...) from a weekly enviro weather file and merge then to a DON file
    arg fileFrom: a weekly enviro weather Excel file
    arg fileTo: the DON file
    """
    
    
    fileFrom = 'from.xlsx'  
    fileTo   = 'to.xlsx'
    
    dfFrom = pd.read_excel(fileFrom, sheetname='Sheet1')
    dfTo   = pd.read_excel(fileTo, sheetname='Sheet1')
    
    yr17Df = dfFrom[ dfFrom['date'].apply(lambda dt: int(dt.split('/')[2])  ) == 2017 ]
    yr18Df = dfFrom[ dfFrom['date'].apply(lambda dt: int(dt.split('/')[2])  ) == 2018 ]
    # the indexes of May. Once we get the last position of May, 
    may17Indxes  = yr17Df[ (yr17Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '05') | (yr17Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '5') ].index.tolist()
    may18Indxes  = yr18Df[ (yr18Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '05') | (yr18Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '5') ].index.tolist()
    # we can find where June starts: the position of the 1st week of June
    jun17SI = may17Indxes[-1] + 1
    jun18SI = may18Indxes[-1] + 1
    # the indexes of September. Once we get the first position of Sept, 
    sep17Indxes  = yr17Df[ (yr17Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '09') | (yr17Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '9') ].index.tolist()
    sep18Indxes  = yr18Df[ (yr18Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '09') | (yr18Df['date'].apply(lambda dt: dt.split('/')[0]  ) == '9') ].index.tolist()
    # we can find where Aug ends: the last week of Aug
    aug17EI = sep17Indxes[0] - 1
    aug18EI = sep18Indxes[0] - 1
    
    # Subset dataframe of only years 2017 & 18: from June - Aug
    df17 = dfFrom.iloc[jun17SI:aug17EI+1, :]
    df18 = dfFrom.iloc[jun18SI:aug18EI+1, :]
    
    
    # making sure that we have 4 weeks for each month
    Mo1Df17 = df17[ (df17['date'].apply(lambda dt: dt.split('/')[0]  ) == '06') | (df17['date'].apply(lambda dt: dt.split('/')[0]  ) == '6') ].reset_index(drop=True) # June
    Mo1Df18 = df18[ (df18['date'].apply(lambda dt: dt.split('/')[0]  ) == '06') | (df18['date'].apply(lambda dt: dt.split('/')[0]  ) == '6') ].reset_index(drop=True)
                  
    Mo2Df17 = df17[ (df17['date'].apply(lambda dt: dt.split('/')[0]  ) == '07') | (df17['date'].apply(lambda dt: dt.split('/')[0]  ) == '7') ].reset_index(drop=True) # July
    Mo2Df18 = df18[ (df18['date'].apply(lambda dt: dt.split('/')[0]  ) == '07') | (df18['date'].apply(lambda dt: dt.split('/')[0]  ) == '7') ].reset_index(drop=True)
                  
    Mo3Df17 = df17[ (df17['date'].apply(lambda dt: dt.split('/')[0]  ) == '08') | (df17['date'].apply(lambda dt: dt.split('/')[0]  ) == '8') ].reset_index(drop=True) # August
    Mo3Df18 = df18[ (df18['date'].apply(lambda dt: dt.split('/')[0]  ) == '08') | (df18['date'].apply(lambda dt: dt.split('/')[0]  ) == '8') ].reset_index(drop=True)
    
    
    colLstF = dfFrom.columns.tolist()
    colLstF.remove('date')
    extLst  = [ # list of extensions to append to the columns: June-August, subdivided in 4 weeks.
               '_JunW1', '_JunW2', '_JunW3', '_JunW4', 
               '_JulW1', '_JulW2', '_JulW3', '_JulW4',
               '_AugW1', '_AugW2', '_AugW3', '_AugW4'  
              ]
    
    # these are the names we'll append in the final excel file. e.g: atmp_max_JunW1, etc...
    colNames = []
    # create the names of the columns
    for col in colLstF:
        colL = []
        for ext in extLst:
            name = col + ext
            colL.append(name)  
        colNames.append(colL)        
     
    # extract the preferable indexes
    dfTo17 = dfTo[ dfTo['Year'] == 2017 ].index.tolist()
    dfTo18 = dfTo[ dfTo['Year'] == 2018 ].index.tolist()
    dfTo17SI = dfTo17[0]   
    dfTo18SI = dfTo18[0]  
    dfTo17EI = dfTo17[-1]  
    dfTo18EI = dfTo18[-1]   
    
    # Here, we go through each new col name, append it to the DON df and fill in with the right values
    for inx in range(len(colNames)): # we iterate through 14 lists. Each list containing 12 names
        
        iMo1 = 0
        iMo2 = 0
        iMo3 = 0
        varCols = colNames[inx] # e.g. atmp_max_{JunW1-AugW4}
        
        for iVar in range(len(varCols)):
            
            colName = varCols[iVar] # e.g. atmp_max_JunW1
            
            if 0 <= iVar <= 3: # JunW1 - W4
                
                val17 = Mo1Df17.iloc[iMo1, inx+1]
                val18 = Mo1Df18.iloc[iMo1, inx+1]       
                #print('=== June ===', val17, val18)
                iMo1 += 1
            
            elif 4 <= iVar <= 7: # JulW1 - W4
                val17 = Mo2Df17.iloc[iMo2, inx+1]
                val18 = Mo2Df18.iloc[iMo2, inx+1]
                #print('=== July ===', val17, val18)
                iMo2 += 1
            
            elif 8 <= iVar <= 11: # AugW1 - W4
                val17 = Mo3Df17.iloc[iMo3, inx+1]
                val18 = Mo3Df18.iloc[iMo3, inx+1]
                #print('=== August ===', val17, val18)
                iMo3 += 1
            
            # Make a column and fill it with the corresponding unique value depending on the year
            dfTo[colName] = np.nan
            dfTo.loc[dfTo17SI:dfTo17EI, colName] = val17
            dfTo.loc[dfTo18SI:dfTo18EI, colName] = val18 
    
            # export to Excel
    fname =  "..\\BigData\\" + fileTo
    dfTo.to_excel(fname, sheet_name='Sheet1')
    

#==============================================================================
# fileFrom = 'from.xlsx'  
# fileTo   = 'to.xlsx'
# 
# mergeFromEnvWeather(fileFrom, fileTo)
#==============================================================================

#MakeBigData(file)


f1 = '1.xlsx'
f2 = '2.xlsx'

lst = ['1.xlsx', '2.xlsx']
dfLst = []
#dfBig = pd.DataFrame()

for f in lst:
    
    df = pd.read_excel(f, sheetname='Sheet1')
    dfLst.append(df)
    
dfBig = pd.concat(dfLst, ignore_index=True)    


